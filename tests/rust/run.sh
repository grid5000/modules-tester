#!/bin/bash

set -ueo pipefail

. ./setup.sh

echo "Rust version is: " $(rustc --version)
echo "Cargo version is: " $(cargo --version)

(cd src/hello_cargo; cargo run | grep "Hello, world!")
