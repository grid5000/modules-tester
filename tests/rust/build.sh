#!/bin/bash

set -eu

. ./setup.sh

(cd src; cargo new hello_cargo; cd hello_cargo; cargo build)
