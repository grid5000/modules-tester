// This is the REAL "hello world" for CUDA!
// It takes the string "Hello ", prints it, then passes it to CUDA with an array
// of offsets. Then the offsets are added in parallel to produce the string "World!"
// By Ingemar Ragnemalm 2010
 
#include <stdio.h>
 
const int N = 16; 
const int blocksize = 16; 
 
__global__ 
void hello(char *a, int *b) 
{
	a[threadIdx.x] += b[threadIdx.x];
}

void error_checking(const char *prefix)
{
	cudaError_t err = cudaGetLastError();
	if (err != cudaSuccess)
	{
		fprintf(stderr, "[%s] Error: %s\n", prefix, cudaGetErrorString(err));
		exit(EXIT_FAILURE);
	}
}

int main()
{
	error_checking("start");

	int device;
	cudaGetDevice(&device);
	error_checking("GetDevice");

	struct cudaDeviceProp props;
	cudaGetDeviceProperties(&props, device);
	error_checking("GetDeviceProperties");

	char a[N] = "Hello \0\0\0\0\0\0";
	int b[N] = {15, 10, 6, 0, -11, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
 
	char *ad;
	int *bd;
	const int csize = N*sizeof(char);
	const int isize = N*sizeof(int);
 
	printf("%s", a);
 
	cudaMalloc( (void**)&ad, csize ); 
	error_checking("Malloc(ad)");
	cudaMalloc( (void**)&bd, isize ); 
	error_checking("Malloc(bd)");
	cudaMemcpy( ad, a, csize, cudaMemcpyHostToDevice ); 
	error_checking("Memcpy(ad, HostToDevice)");
	cudaMemcpy( bd, b, isize, cudaMemcpyHostToDevice ); 
	error_checking("Memcpy(bd, HostToDevice)");

	dim3 dimBlock( blocksize, 1 );
	dim3 dimGrid( 1, 1 );
	hello<<<dimGrid, dimBlock>>>(ad, bd);
	error_checking("Kernel");
	cudaMemcpy( a, ad, csize, cudaMemcpyDeviceToHost ); 
	error_checking("Memcpy(ad, DeviceToHost)");
	cudaFree( ad );
	error_checking("Free(ad)");
	cudaFree( bd );
	error_checking("Free(bd)");

	printf("%s\n", a);
	fprintf(stderr, "PCIBusID(%d),PCIDeviceID(%d)\n", props.pciBusID, props.pciDeviceID);
	return EXIT_SUCCESS;
}
