#!/bin/bash

set -ueo pipefail

. ./setup.sh

nb_gpu=$(jq '[.gpu_devices[]]|length' /etc/grid5000/ref-api.json)

nvcc --version

for n in $(seq 1 $nb_gpu)
do
	src/hello-world | grep -o "^Hello World!"
done

