#!/bin/bash
set -u

. ./setup.sh

nb_gpu=$(jq '[.gpu_devices[]]|length' /etc/grid5000/ref-api.json)

STARPU_NCUDA=$nb_gpu starpu_machine_display | grep -o "^$nb_gpu STARPU_CUDA_WORKER workers:"
