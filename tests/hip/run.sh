#!/bin/bash

set -ueo pipefail

. ./setup.sh

nb_gpu=$(jq '[.gpu_devices[]]|length' /etc/grid5000/ref-api.json)

hipcc --version

for n in $(seq 1 $nb_gpu)
do
	echo "Testing GPU $n"
	src/HelloWorld | grep -o "^Passed!"
done
