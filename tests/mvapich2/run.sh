#!/bin/bash

set -ueo pipefail

. ./setup.sh

echo -n "Path of mpirun: "
which mpirun
[ $(which mpirun) != '/usr/bin/mpirun' ]
echo -n "Version of mpirun: "
mpirun --version
echo -n "Smoke test: "
mpirun -machinefile $OAR_NODEFILE -np 8 $PWD/src/mpi_hello_world
