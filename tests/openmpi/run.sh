#!/bin/bash

set -ueo pipefail

. ./setup.sh

echo "Path of ompi_info: " $(which ompi_info)
echo "Checking that the -enable-mpirun-prefix-by-default is used: "
ompi_info | grep -- "--enable-mpirun-prefix-by-default"
