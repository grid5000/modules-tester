#!/bin/bash

set -u

. ../../setup.sh

if [ $# != 2 ] ; then
  usage_setup
fi

load_module $1 $2
