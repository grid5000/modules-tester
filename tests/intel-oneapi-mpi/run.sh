#!/bin/bash

set -ueo pipefail

. ./setup.sh

echo -n "Path of mpirun: "
which mpirun
[ $(which mpirun) != '/usr/bin/mpirun' ]
echo -n "Version of mpirun: "
mpirun --version
echo -n "Check that Intel MPI is used: "
mpirun -machinefile $OAR_NODEFILE -genv I_MPI_DEBUG=+5 -genv I_MPI_PIN_PROCESSOR_LIST allcores -np 8 $PWD/src/mpi_hello_world|grep "Intel(R) MPI Library"
