#!/bin/bash

set -ueo pipefail

. ./setup.sh

src/clang
echo "def foo(x y) x+foo(y, 4.0);"|src/llvm 2>&1 | grep -o 'Parsed a function definition.'
