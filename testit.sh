#!/bin/bash

set -ue

progname=$(basename $0)
basedir=$(realpath $(dirname $0))

function usage {
  echo "usage: ${progname} module_to_be_tested version"
  exit 0
}

function die {
  >&2 echo "${progname}: $1"
  exit 1
}

if [ $# != 2  ] ; then
  usage
fi

module=$1
module_dir="${basedir}/tests/${module}"
version=$2

if [ ! -d "${module_dir}" ] ; then
  die "${module} is not part of the test set"
fi

for file in setup.sh run.sh
do
  if [ ! -f "${module_dir}/${file}" ] ; then
    die "${file} is missing for ${module}"
  fi
done

cd $module_dir

$basedir/clean.sh

if [ -f "build.sh" ] ; then
  ./build.sh $module $version
fi

./run.sh $module $version
